'use strict';
const bcrypt = require('bcrypt-nodejs');

module.exports = (sequelize, DataTypes) => {
    const UserModel = sequelize.define('user', {
        username: DataTypes.STRING,
        password: DataTypes.STRING
    }, {
        indexes: [
            {
                unique: true,
                fields: ['username']
            },
        ],
        classMethods: {
            associate: (models) => {
            }
        }
    });
    UserModel.beforeCreate(function(user) {
        return new Promise(function (resolve, reject) {
            bcrypt.genSalt(5, function (err, salt) {
                if (err) return callback(err);
                bcrypt.hash(user.password, salt, null, function (err, hash) {
                    if (err) return reject(err);
                    user.password = hash;
                    resolve();
                });
            });
        });
    });
    UserModel.prototype.verifyPassword = function(password) {
        console.log('verifyPassword', password, this.password);
        return new Promise((resolve, reject) => {
            console.log('compare', password, this.password);
            bcrypt.compare(password, this.password, function (err, res) {
                console.log('err', err, res);
                err ? reject(err) : resolve(res);
            });
        });
    };
    return UserModel;
};