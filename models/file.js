'use strict';
module.exports = (sequelize, DataTypes) => {
    const FileModel = sequelize.define('file', {
        file_name: DataTypes.STRING,
        original_name: DataTypes.STRING,
        filesize: DataTypes.STRING,
        type: DataTypes.STRING
    }, {
        indexes: [
            {
                unique: true,
                fields: ['file_name']
            },
        ],
        classMethods: {
            associate: (models) => {
            }
        }
    });
    // UserModel.beforeCreate(function(user) {
    //     return new Promise(function (resolve, reject) {
    //         bcrypt.genSalt(5, function (err, salt) {
    //             if (err) return callback(err);
    //             bcrypt.hash(user.password, salt, null, function (err, hash) {
    //                 if (err) return reject(err);
    //                 user.password = hash;
    //                 resolve();
    //             });
    //         });
    //     });
    // });
    return FileModel;
};