'use strict';

let fs = require('fs');
let path = require('path');
let Sequelize = require('sequelize');
let env = process.env.NODE_ENV || 'development';
let config = require(__dirname + '/config.js').database[env];
let db = {};

let sequelize = config.use_env_variable ?
    new Sequelize(process.env[config.use_env_variable], config) :
    new Sequelize(config.database, config.username, config.password, config);

fs.readdirSync(__dirname + '/models/').filter(file => {
    return file.slice(-3) === '.js';
}).forEach(file => {
    const model = sequelize['import'](path.join(__dirname, 'models', file));
    db[model.name] = model;
});

Object.keys(db).forEach(modelName => {
    db[modelName].associate && db[modelName].associate(db);
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
