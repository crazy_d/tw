const fs = require('fs');

module.exports = function (app) {
    fs.readdirSync(__dirname + '/routes/').filter(file => {
        return file.slice(-3) === '.js';
    }).forEach(file => {
        app.use(require('./routes/' + file.substr(0, file.indexOf('.'))));
    });
};