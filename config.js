let path = require('path');

module.exports = {
    'secret': 'secretpassphrase',
    'database': {
        "development": {
            "username": "postgres",
            "password": "password",
            "database": "postgres",
            "host": "127.0.0.1",
            "dialect": "postgres"
        },
        "test": {
            "username": "root",
            "password": null,
            "database": "database_test",
            "host": "127.0.0.1",
            "dialect": "mysql"
        },
        "production": {
            "username": "root",
            "password": null,
            "database": "database_production",
            "host": "127.0.0.1",
            "dialect": "mysql"
        }
    },
    'files': {
        stordir: path.join(__dirname, 'files'),
        tempdir: path.join(__dirname, 'temp')
    }
};