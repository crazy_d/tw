const express = require('express');
const jwt = require('jsonwebtoken');
const models = require('../models');

const router = express.Router();
const config = require('../config');

router.get('/', (req, res, next) => {
    res.send({message: 'Welcome!'});
});

router.post('/restore', function (req, res) {
    models.user.findOne({
        where: {
            username: req.body.username
        }
    }).then(user => {
        res.json({
            success: true,
            token: jwt.sign({
                username: user.username,
                admin: true
            }, config.secret, {
                expiresIn: 1440 * 60
            })
        });
    }).catch(err => {
        res.send({success: false, message: 'Authentication failed. User not found.', error: err});
    });
});

router.post('/auth', function (req, res) {
    models.user.findOne({
        where: {
            username: req.body.username
        }
    }).then(user => {
        user.verifyPassword(req.body.password).then(() => {
            res.json({
                success: true,
                token: jwt.sign({
                    username: user.username,
                    admin: true
                }, config.secret, {
                    expiresIn: 1440 * 60
                })
            });
        }).catch(err => {
            res.send({success: false, message: 'Authentication failed. Wrong password.'});
        });
    }).catch(err => {
        res.send({success: false, error: err});
    });
});


router.post('/register', function (req, res) {
    models.user.create({
        username: req.body.username,
        password: req.body.password,
    }).then(user => {
        res.send({success: true});
    }).catch(err => {
        res.send({success: false});
    });
});

module.exports = router;
