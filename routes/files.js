let express = require('express');
let router = express.Router();
let path = require('path');
const multer = require('multer');
let config = require('../config');
let Recaptcha = require('express-recaptcha');
let recaptcha = new Recaptcha('6LdPkkQUAAAAAOmgo-eCksZeD57yd6fxJoQhlxbi', '6LdPkkQUAAAAAIeudafotgsZ6ePr5MqXs5L5ktJ_');
const fs = require('fs-extra');
const models = require('../models');

const upload = multer({
    storage: multer.diskStorage({
        destination: (req, file, cb) => {
            cb(null, config.files.tempdir)
        },
    })
});

router.post('/protected/files/delete', (req, res) => {
    models.file.findById(req.body.id).then(file => {
        file.destroy().then(() => {
            const filepath = path.join(config.files.stordir, file.file_name);
            if (fs.existsSync(filepath)) {
                fs.unlink(filepath, (err) => {
                    if (err) {
                        res.send({success: false, error: err});
                    } else {
                        res.send({success: true});
                    }
                });
            } else {
                res.send({success: true});
            }
        }).catch(err => {
            res.send({success: false, error: err});
        });
    }).catch(err => {
        res.send({success: false, error: err});
    });
});

router.post('/protected/files/upload', upload.single('file'), (req, res) => {
    recaptcha.verify(req, function (err, data) {
        if (!err) {
            models.file.create({
                original_name: req.file.originalname,
                file_name: req.file.filename,
                filesize: req.file.size,
                type: req.file.mimetype
            }).then(file => {
                fs.move(req.file.path, path.join(config.files.stordir, req.file.filename), function (err) {
                    if (err) {
                        res.send({success: false, error: err});
                    } else {
                        res.send({success: true, file: file});
                    }
                });
            }).catch(err => {
                res.send({success: false, error: err});
            });
        } else {
            res.send({success: false, error: err, data: data});
        }
    });
});

router.get('/protected/files/:file_name/:original_name', function (req, res) {
    models.file.findOne({
        where: {
            file_name: req.params.file_name,
            original_name: req.params.original_name
        }
    }).then(file => {
        res.sendFile(path.join(config.files.stordir, file.file_name));
    }).catch(err => {
        res.send({success: false, error: err});
    });
});


router.get('/protected/files/', (req, res) => {
    models.file.findAll({}).then(files => {
        res.send({success: true, files: files});
    }).catch(err => {
        res.send({success: false, files: [], error: err});
    });
});

module.exports = router;
